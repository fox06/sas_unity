﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveObject : MonoBehaviour
{
    private bool _selected;
      
    public bool Selected
    {
        get { return _selected; }
    }

    public bool Swap;

    public void Select()
    {
        _selected = true;

        foreach (var selection in GetComponents<Interaction>())
        {
            selection.Select();
        }
    }

    public void Deselect()
    {
        _selected = false;

        foreach (var selection in GetComponents<Interaction>())
        {
            selection.Deselect();
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	    if (Swap)
	    {
	        Swap = false;

	        if (_selected)
	            Deselect();
	        else
	            Select();

	    }
	}
}
