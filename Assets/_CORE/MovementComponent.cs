﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;

public class MovementComponent : MonoBehaviour
{
    public delegate void OnDestinationReached();

    public event OnDestinationReached DestinationReached;

    public delegate void OnWaypointReached();

    public event OnWaypointReached WaypointReached;

    [SerializeField] float moveSpeed;
    [SerializeField] float rotateSpeed;

    List<Vector3> path = new List<Vector3>();

    bool InAction = false;
 
    public bool IsInAction { get { return InAction; } }

    public void Reset()
    {
        path.Clear();
    }

    public void SetWaypoints(List<Vector3> waypoints)
    {
        path = waypoints;
    }

    public void Push()
    {
        StartCoroutine(MoveCoroutine(path, true));
    }
    public void Move()
    {
        StartCoroutine(MoveCoroutine(path));
    }
      
    public IEnumerator MoveCoroutine(List<Vector3> way, bool withoutRotation = false)
    {
        InAction = true;

        foreach (var targetPos in way)
        {
            if (!withoutRotation)
                yield return StartCoroutine(RotateToCoroutine(targetPos));


            Vector3 startPos = transform.position;
            Vector3 endPos = targetPos;

            var dist = (startPos - endPos).sqrMagnitude;

            while (dist > 0.01f)
            {
                transform.position = Vector3.MoveTowards(transform.position, endPos, moveSpeed * Time.deltaTime);
                yield return null;

                dist = (transform.position - endPos).sqrMagnitude;
            }

            transform.position = endPos;

            if (WaypointReached != null)            
                WaypointReached();
            
            
        }

        if (DestinationReached != null)
            DestinationReached();

        InAction = false;
    }

    public IEnumerator RotateToCoroutine(Vector3 target)
    {
        Vector3 lookDirection = target - transform.position;


        Quaternion targetRotation = Quaternion.LookRotation(lookDirection);

        while (!IsFacingObject(lookDirection))
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotateSpeed *  Time.deltaTime);

            yield return null;
        }

        transform.rotation = targetRotation;

    }
    
    private bool IsFacingObject(Vector3 lookDirection)
    {
        Vector3 forward = transform.forward;
        Vector3 toOther = lookDirection.normalized;

        if (Vector3.Dot(forward, toOther) < .99f)
        {
            return false;
        }

        return true;
    }

    public void AddWaypoint(Vector3 destination)
    {
        path.Add(destination);
    }
}
