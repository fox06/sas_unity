﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatHighlighter : MonoBehaviour
{
    
    public GameObject gm;
    public void Select()
    {
        gm.SetActive(true);
    }

    public void Deselect()
    {
        gm.SetActive(false);
    }

    void Start()
    {

        gm.SetActive(false);
    }

    void Update()
    {

    }
}
