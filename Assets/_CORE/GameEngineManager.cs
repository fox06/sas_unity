﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using SAS.Map;
using SAS.CameraUI;


//using Highlighter = UnityEditor.Highlighter;

public class GameEngineManager : MonoBehaviour {
      
      
    public static GameEngineManager Instance = null;

    public List<PlayerSetting> playersSettings = new List<PlayerSetting>();
    
    private TileMap map;

    private PlayerSetting currentPlayerTurn;

    public PlayerSetting CurrentPlayerTurn { get { return currentPlayerTurn; } }

    public int PlayerLayerIndex { get { return CurrentPlayerTurn.PlayerLayerIndex; } }

    void Awake()
    {
        Instance = this;
    }

   
    void Start () {

        currentPlayerTurn = playersSettings[0];

        map = FindObjectOfType<TileMap>();


        foreach (var playersSetting in playersSettings)
        {
            foreach (var unit in playersSetting.ActiveUnits)
            {
                if (unit == null)
                    continue;

                var flag = Instantiate(playersSetting.flagPrefab, Vector3.zero, Quaternion.identity);

                flag.transform.SetParent(unit.FlagSocket, false);

                flag.GetComponent<Renderer>().material.color = playersSetting.playerColor;
          
            }
        }

        OnNextTurn();

    }
    
    public void NextTurn()
    {
        int currPlayerInd = playersSettings.IndexOf(currentPlayerTurn);

        if (playersSettings.Count - 1 == currPlayerInd)
        {
            currentPlayerTurn = playersSettings[0];
        }
        else
        {
            currentPlayerTurn = playersSettings[currPlayerInd + 1];
        }



        OnNextTurn();
   

    }

    public void OnNextTurn()
    {
        Wind.SetRandomWind();

        GameInfoManager.Instance.SetTurnPlayerName(currentPlayerTurn.Name);
        GameInfoManager.Instance.SetWindDirection(Wind.WindDiscriptionProp.Direction.ToString());
        GameInfoManager.Instance.SetWindType(Wind.WindType.ToString());


        foreach (var playersSetting in playersSettings)
        {
            foreach (var unit in playersSetting.ActiveUnits)
            {
                if (unit == null)
                    continue;

                unit.ResetPoints(Wind.WindType);

                var rapairUnit = unit.GetComponent<Repairer>();

                if (rapairUnit != null)
                {
                    rapairUnit.Repair();
                }
            }
        }

        CameraRaycaster cameraRaycaster = FindObjectOfType<CameraRaycaster>();
        cameraRaycaster.SetPlayerLayer(currentPlayerTurn.PlayerLayerIndex);
 

        if (Wind.WindType == Wind.WindTypes.Storm)
        {
            foreach (var unit in currentPlayerTurn.ActiveUnits)
            {
                var movementor = unit.GetComponent<MovementComponent>();

                if (movementor == null)
                    return;

                movementor.Reset();
                movementor.DestinationReached += () => FindObjectOfType<InteractionSystem>().Select(currentPlayerTurn.ActiveUnits[0]);

                var mo = unit.GetComponent<MapObject>();
                var node = mo.CurrentNode;

                var windVec = Wind.WindDiscriptionProp.Vector;

                int stormMoves = unit.StormMovePoints;

                for (int i = 1; i < stormMoves; i++)
                {
                    int newNodeX = node.x + (int)windVec.x;
                    int newNodeY = node.y + (int)windVec.y;
                    int newNodeZ = node.z + (int)windVec.z;

                    var newNode = map.GetTile(newNodeX, newNodeY, newNodeZ);

                    if (newNode == null)
                        break;

                   

                    if (newNode.IsWalkable)
                        movementor.AddWaypoint(map.TileCoords(newNode.x, newNode.z));

                }

                movementor.Push();

                


            }
        }
        else
        {
            FindObjectOfType<InteractionSystem>().Select(currentPlayerTurn.ActiveUnits[0]);
        }

        

    }

    public void NextUnit()
    {
        var interactionSystem = FindObjectOfType<InteractionSystem>();

        var selectedUnit = interactionSystem.SelectedObject;

        var unitsCount = currentPlayerTurn.ActiveUnits.Count;

        var ind = -1;

        if (selectedUnit == null)
        {
            ind = 0;
        }
        else
        {
            ind = currentPlayerTurn.ActiveUnits.IndexOf(selectedUnit);
        }

      
        for (var z = 1; z < unitsCount; ++z)
        {
            var idx = (z + ind) % unitsCount;

            var selected = currentPlayerTurn.ActiveUnits[idx];

            if (selected != null)
            {
                interactionSystem.Select(selected);
                break;
            }
        }
               
       
        
    }

    public void PrevUnit()
    {
        var interactionSystem = FindObjectOfType<InteractionSystem>();

        var selectedUnit = interactionSystem.SelectedObject;

        var unitsCount = currentPlayerTurn.ActiveUnits.Count;

        var ind = -1;

        if (selectedUnit == null)
        {
            ind = 0;
        }
        else
        {
            ind = currentPlayerTurn.ActiveUnits.IndexOf(selectedUnit);
        }


        for (var z = 1; z < unitsCount; ++z)
        {
            var idx = ind - z;

            if (idx < 0)
                idx = unitsCount -1 - ind;
             

            var selected = currentPlayerTurn.ActiveUnits[idx];

            if (selected != null)
            {
                interactionSystem.Select(selected);
                break;
            }
        }




    }




}



