﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[System.Serializable]
public class PlayerSetting
{
    [SerializeField] public string Name;

    [SerializeField] public int PlayerLayerIndex;

    [SerializeField]
    public Color playerColor;

    [SerializeField]
    public GameObject flagPrefab;

    [SerializeField] List<Unit> Units;

    [SerializeField] GameObject UnitsPlaceholder;

    
    public List<Unit> ActiveUnits { get { return Units.Where(x => x != null).ToList(); } }


}
