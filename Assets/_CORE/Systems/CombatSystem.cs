﻿using SAS.Map;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CombatSystem : MonoBehaviour {

    [SerializeField] GameObject EnemySelectorPrefab;
    [SerializeField] GameObject FireTextPrefab;

    GameObject EnemySelector;

    [SerializeField] GameObject CombatPanel;

    [SerializeField] Button SingleAttackButton;
    [SerializeField] Button VolleyAttackButton;
    [SerializeField] Button GroupAttackButton;

    //[SerializeField] GameObject EnemyPrefab;

    List<InteractiveObject> EnemiesInRange = new List<InteractiveObject>();

    List<Unit> PotentialAttackersOfTarget = new List<Unit>();

    List<Tile> EnemiesTiles = new List<Tile>();

    List<GameObject> EnemyTilesGO = new List<GameObject>();

    Unit Attacker;
    Unit Target;

    TileMap map;

    Tile selectedOUnitTile;
    private bool IsBusy;

    float Accuracy = 0.6f;

    // Use this for initialization
    void Start()
    {

        CombatPanel.SetActive(false);

        SingleAttackButton.onClick.AddListener(delegate { StartCoroutine(AttackAction(Attacker, 1)); });
        VolleyAttackButton.onClick.AddListener(delegate { StartCoroutine(AttackAction(Attacker, Attacker.AttackPoints())); });
        GroupAttackButton.onClick.AddListener(delegate  { StartCoroutine(GroupAttackAction()); });

        map = FindObjectOfType<TileMap>();
        //EnemySelector = Instantiate(EnemySelectorPrefab);
        //EnemySelector.SetActive(false);

    }

    public void InitializeForUnit(Unit unit)
    {
        Attacker = unit;
        Tile originTile = map.GetNodeByWorldPosition(unit.transform.position);

        ClearAll();

        CalculateEnemies(originTile, unit.AttackRange());

        LocateSelectors();
    }

    public void SelectEnemy(Unit target)
    {  
        Target = target;

        // SelectorPrefab

        //  CombatPanel.gameObject.transform.SetParent(Target.GetComponentInChildren<IndicatorUI>().transform, false);

        CombatPanel.gameObject.transform.position = Target.GetComponentInChildren<IndicatorUI>().transform.position;
        CombatPanel.SetActive(true);

        var enemySelector = target.gameObject.GetComponentInChildren<Selector>();

        if (enemySelector != null)
        {
            enemySelector.Select();
        }

        CalculatePotentialAttackersFor(target);
    }

    private void CalculatePotentialAttackersFor(Unit target)
    {
        PotentialAttackersOfTarget.Clear();

        var player = GameEngineManager.Instance.CurrentPlayerTurn;

        var playerUnits = player.ActiveUnits;

        foreach (var unit in playerUnits)
        {
            //TODO Проверить расстояние в тайлах от цели

            if (unit.AttackPoints() == 0)
                continue;


            var originTile = unit.GetComponent<MapObject>().CurrentNode;

            var attackRange = unit.AttackRange();

            MapObject enemyTile = target.GetComponent<MapObject>();
            InteractiveObject enemyIO = target.GetComponent<InteractiveObject>();


            var disX = originTile.x - enemyTile.CurrentNode.x;
            var disZ = originTile.z - enemyTile.CurrentNode.z;

            if (disX == 0 && Math.Abs(-disZ) <= attackRange)
            {
               // EnemiesTiles.Add(enemyTile.CurrentNode);
                PotentialAttackersOfTarget.Add(unit);
            }

            if (disZ == 0 && Math.Abs(-disX) <= attackRange)
            {
               // EnemiesTiles.Add(enemyTile.CurrentNode);
                PotentialAttackersOfTarget.Add(unit);
            }


            if (-disX <= attackRange)
                if (disX == disZ)
                {
                    //EnemiesTiles.Add(enemyTile.CurrentNode);
                    PotentialAttackersOfTarget.Add(unit);
                }

            if (disX >= -attackRange)
                if (disX == -disZ)
                {
                   // EnemiesTiles.Add(enemyTile.CurrentNode);
                    PotentialAttackersOfTarget.Add(unit);
                }

        }


    }

    public void DeselectEnemy()
    {
        if (Target != null)
        {
            Target.Deselect();

            var enemySelector = Target.gameObject.GetComponentInChildren<Selector>();

            if (enemySelector != null)
            {
                enemySelector.Deselect();
            }
        }

        if (CombatPanel != null)
        {
            CombatPanel.SetActive(false);
        }      
        

        


    }



    void CalculateEnemies(Tile originTile, int attackRange)
    {
        foreach (var playersSetting in GameEngineManager.Instance.playersSettings)
        {
            if (playersSetting == GameEngineManager.Instance.CurrentPlayerTurn)
                continue;

            foreach (var enemyUnit in playersSetting.ActiveUnits)
            {
                if (enemyUnit == null)
                    continue;

                MapObject enemyTile = enemyUnit.GetComponent<MapObject>();
                InteractiveObject enemyIO = enemyUnit.GetComponent<InteractiveObject>();


                var disX = originTile.x - enemyTile.CurrentNode.x;
                var disZ = originTile.z - enemyTile.CurrentNode.z;

                if (disX == 0 && Math.Abs(-disZ) <= attackRange)
                {
                    EnemiesTiles.Add(enemyTile.CurrentNode);
                    EnemiesInRange.Add(enemyIO);
                }

                if (disZ == 0 && Math.Abs(-disX) <= attackRange)
                {
                    EnemiesTiles.Add(enemyTile.CurrentNode);
                    EnemiesInRange.Add(enemyIO);
                }


                if (-disX <= attackRange)
                    if (disX == disZ)
                    {
                        EnemiesTiles.Add(enemyTile.CurrentNode);
                        EnemiesInRange.Add(enemyIO);
                    }

                if (disX >= -attackRange)
                    if (disX == -disZ)
                    {
                        EnemiesTiles.Add(enemyTile.CurrentNode);
                        EnemiesInRange.Add(enemyIO);
                    }

            }

        }
    }

    private void LocateSelectors()
    {
        foreach (var item in EnemiesInRange)
        {     
            EnemyTilesGO.Add(Instantiate(EnemySelectorPrefab, item.transform.position, Quaternion.identity, item.transform));
        }
               
    }

    public void ClearAll()
    {
      
        EnemiesInRange.Clear();
        EnemiesTiles.Clear();
        PotentialAttackersOfTarget.Clear();     

        foreach (var item in EnemyTilesGO)
        {
            Destroy(item);
        }

        DeselectEnemy();
    }

    internal bool IsEnemyInRange(InteractiveObject interactiveObject)
    {
        if (EnemiesInRange == null)
            return false;

        return EnemiesInRange.Contains(interactiveObject);
    }

    private IEnumerator GroupAttackAction()
    {
        foreach (var attacker in PotentialAttackersOfTarget)
        {
            yield return AttackAction(attacker, 1);
        }
        
    }
    private IEnumerator AttackAction(Unit Attacker, int attackPoints)
    {
        IsBusy = true;

       

            if (Target == null || Target.IsDead() || Attacker.AttackPoints() <= 0)
            {
            yield break;
            }

        yield return new WaitForSeconds(0.5f);
                             
            var r = UnityEngine.Random.Range(0f, 1f);

            if (r < Accuracy)
            {
                ShowFireText("MISS!!!");
            }
            else
            {
                ShowFireText("-" + attackPoints.ToString());
               
                Target.TakeDamage(attackPoints);
            }

            Attacker.DecreaseAttackPoints(attackPoints);

            if (Target.IsDead())
            {
                CombatPanel.SetActive(false);
                //CombatPanel.gameObject.transform.SetParent(null, false);


                Target.Kill();

                   Target = null;


           
        }

        IsBusy = false;
    }

    public void ShowFireText(string fireText)
    {
        var text = Instantiate(FireTextPrefab, Vector3.zero, Quaternion.identity);

        text.transform.SetParent(CombatPanel.transform, false);

        text.GetComponentInChildren<FadableText>().BeginAction(fireText);

    }
   
}
