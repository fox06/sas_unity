﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SAS.CameraUI;
using System;
using SAS.Map;

public class InteractionSystem : MonoBehaviour {
        
        
    [SerializeField] CombatSystem CombatSystem;
    [SerializeField] ActionSystem ActionSystem;

    GameObject playerSelector;
    [SerializeField] GameObject playerSelectorPrefab;


    Unit selecteUnit = null;
       

    Dictionary<Tile, int> availablePath = new  Dictionary<Tile, int>();

    TileMap map;

    private bool IsBusy;

    public Unit SelectedObject
    {
        get
        {
            return selecteUnit;
        }
      
    }

    void Start () {
        CameraRaycaster cameraRaycaster = FindObjectOfType<CameraRaycaster>();

        cameraRaycaster.onMouseOverFriendInteractiveObject += OnMouseOverFriendInteractiveObject;
        cameraRaycaster.onMouseOverEnemyInteractiveObject += OnMouseOverEnemyInteractiveObject;
        cameraRaycaster.onMouseOverPotentiallyWalkable += OnMouseOverPotentiallyWalkable;

        map = FindObjectOfType<TileMap>();

        playerSelector = Instantiate(playerSelectorPrefab);
        playerSelector.SetActive(false);


        
            
    }


    private void OnMouseOverPotentiallyWalkable(Vector3 destination)
    {
        if (Input.GetMouseButtonDown(0) && !IsBusy)
        {
            Tile clickedTile = map.GetNodeByWorldPosition(destination);

            if (clickedTile == null)
                return;
            
            if (selecteUnit != null )
            {

                List<Tile> pathTile = ActionSystem.CalculateDestinationPath(clickedTile);
                List<Vector3> path = new List<Vector3>();

                foreach (var item in pathTile)
                {
                    path.Add(map.TileCoords(item.x, item.z));
                }

                if (path.Count > 0)
                {
                    IsBusy = true;
                    var movementor = SelectedObject.GetComponent<MovementComponent>();
                    movementor.SetWaypoints(path);
                    movementor.Move();
                    movementor.DestinationReached += Movementor_DestinationReached;
                    movementor.WaypointReached += Movementor_WaypointReached;

                }
                           

            }
            else
            {
                Deselect();
                DeselectEnemy();
            }
        }
    }

    private void Movementor_WaypointReached()
    {
        selecteUnit.MovePoints--;
    }

    private void Movementor_DestinationReached()
    {
        IsBusy = false;
        Select(SelectedObject);
    }

    private void OnMouseOverEnemyInteractiveObject(InteractiveObject interactiveObject)
    {
        if (Input.GetMouseButtonDown(0) && SelectedObject != null && !IsBusy)
        {
            if(CombatSystem.IsEnemyInRange(interactiveObject))
                SelectEnemy(interactiveObject as Unit);
        }
    }

    private void OnMouseOverFriendInteractiveObject(InteractiveObject interactiveObject)
    {
        if (Input.GetMouseButtonDown(0) && !IsBusy)
        {
            Select(interactiveObject as Unit);

        }
    }

    public void Deselect()
    {
        if (selecteUnit != null)
        {

            ActionSystem.ClearAll();

            CombatSystem.ClearAll();
            selecteUnit.Deselect();

            playerSelector.SetActive(false);

            selecteUnit = null;

            availablePath.Clear();
        }
    }

    public void Select(Unit interactiveObject) 
    {
        Deselect();
        
        if (interactiveObject != null)
        {
            selecteUnit = interactiveObject;
            selecteUnit.Select();

            ActionSystem.InitializeForUnit(selecteUnit);
            CombatSystem.InitializeForUnit(selecteUnit);

            playerSelector.SetActive(true);
            playerSelector.transform.SetParent(selecteUnit.transform, false);

            var playerSelectorComp = playerSelector.GetComponent<Selector>();

            if (playerSelectorComp != null)
            {
                playerSelectorComp.Select();
            }
        }
    }

    void SelectEnemy(Unit interactiveObject)
    {
        CombatSystem.SelectEnemy(interactiveObject);
    }

    void DeselectEnemy()
    {
        CombatSystem.DeselectEnemy();     
    }


   

}
