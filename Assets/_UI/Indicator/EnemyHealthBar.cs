﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealthBar : MonoBehaviour
{  
    Unit unitStats = null;
    Text text = null;

    int maxHealth;
    

    // Use this for initialization
    void Start()
    {
        unitStats = GetComponentInParent<Unit>(); // Different to way player's health bar finds player
        text = GetComponent<Text>();
        maxHealth = unitStats.MaxHealth();
    }

    // Update is called once per frame
    void Update()
    {
        text.text = string.Format("{0}/{1}/{2}/{3}", unitStats.Health().ToString(), maxHealth.ToString(), unitStats.MovePoints.ToString(), unitStats.AttackPoints());
    }
}
