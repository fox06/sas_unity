﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {


    public float moveSpeed;
    public float rotSpeed;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        Movement();
        Rotation();
        Zoom();
    }


    void Zoom()
    {
        bool zoomIn = Input.GetKey(KeyCode.Z);
        bool zoomOut = Input.GetKey(KeyCode.X);

        Vector3 targetPosition = new Vector3();

        if (zoomIn || zoomOut)
        {
            float value = moveSpeed;

            if (zoomIn)
                value = -moveSpeed;


            transform.position += transform.forward * value;

        }


    }

    void Rotation()
    {
        bool rotateLeft = Input.GetKey(KeyCode.Q);
        bool rotateRight = Input.GetKey(KeyCode.E);



        if (rotateLeft || rotateRight)
        {
            float value = rotSpeed;

            if (rotateLeft)
                value = -value;

            Vector3 euler = transform.localEulerAngles;

            euler.y += value;

            transform.localEulerAngles = euler;
           
        }

       
    }

    void Movement()
    {

        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        

        Vector3 targetPosition = new Vector3();

        if (horizontal != 0)
            targetPosition += horizontal * transform.right;

        if (vertical != 0)
            targetPosition += vertical * new Vector3(transform.forward.x, 0, transform.forward.z);

        transform.position += targetPosition * moveSpeed;
    }
}
