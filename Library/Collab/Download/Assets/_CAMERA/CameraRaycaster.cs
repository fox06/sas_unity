﻿﻿using UnityEngine;
using UnityEngine.EventSystems;


namespace SAS.CameraUI
{
    public class CameraRaycaster : MonoBehaviour
    {
		[SerializeField] Texture2D walkCursor = null;
        [SerializeField] Texture2D enemyCursor = null;
        [SerializeField] Texture2D friendCursor = null;


        [SerializeField] Vector2 cursorHotspot = new Vector2(0, 0);

        const int POTENTIALLY_WALKABLE_LAYER = 8;

        int PlayerLayer = 0;

        float maxRaycastDepth = 100f; // Hard coded value

        Rect currentScrenRect;
        
        public delegate void OnMouseOverFriendInteractiveObject(InteractiveObject interactiveObject);
        public event OnMouseOverFriendInteractiveObject onMouseOverFriendInteractiveObject;

        public delegate void OnMouseOverEnemyInteractiveObject(InteractiveObject interactiveObject);
        public event OnMouseOverEnemyInteractiveObject onMouseOverEnemyInteractiveObject;

        public delegate void OnMouseOverTerrain(Vector3 destination);
        public event OnMouseOverTerrain onMouseOverPotentiallyWalkable;


        void Update()
        {
            currentScrenRect = new Rect(0, 0, Screen.width, Screen.height);

            // Check if pointer is over an interactable UI element
            if (EventSystem.current.IsPointerOverGameObject())
            {
                // Impliment UI interaction
            }
            else
            {
                PerformRaycasts();
            }
        }

        void PerformRaycasts()
		{
            if (currentScrenRect.Contains(Input.mousePosition))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                PlayerLayer = GameEngineManager.Instance.PlayerLayerIndex;

                // Specify layer priorities below, order matters
                if (RaycastForFriend(ray)) { return; }
                if (RaycastForEnemy(ray)) { return; }
                if (RaycastForPotentiallyWalkable(ray)) { return; }
            }
		}

        bool RaycastForFriend(Ray ray)
        {
            RaycastHit hitInfo;
            Physics.Raycast(ray, out hitInfo, maxRaycastDepth);

            if (hitInfo.collider == null)
            {
                return false;
            }

            var gameObjectHit = hitInfo.collider.gameObject;
            var friendHit = gameObjectHit.GetComponent<InteractiveObject>();

            LayerMask potentiallyfriendLayer = 1 << PlayerLayer;
            bool potentiallyfriendLayerHit = Physics.Raycast(ray, out hitInfo, maxRaycastDepth, potentiallyfriendLayer);

            if (potentiallyfriendLayerHit)
            {
                Cursor.SetCursor(friendCursor, cursorHotspot, CursorMode.Auto);

                if (onMouseOverFriendInteractiveObject != null)
                {
                    onMouseOverFriendInteractiveObject(friendHit);
                }
        
                return true;
            }
            return false;
        }

        bool RaycastForEnemy(Ray ray)
		{
            RaycastHit hitInfo;
            Physics.Raycast(ray, out hitInfo, maxRaycastDepth);

            if (hitInfo.collider == null)
            {
                return false;
            }

            var gameObjectHit = hitInfo.collider.gameObject;
            var enemyHit = gameObjectHit.GetComponent<InteractiveObject>();
            if (enemyHit)
            {
                Cursor.SetCursor(enemyCursor, cursorHotspot, CursorMode.Auto);
                onMouseOverEnemyInteractiveObject(enemyHit);
                return true;
            }
            return false;
		}

        private bool RaycastForPotentiallyWalkable(Ray ray)
        {
            RaycastHit hitInfo;
            LayerMask potentiallyWalkableLayer = 1 << POTENTIALLY_WALKABLE_LAYER;

            bool potentiallyWalkableHit = Physics.Raycast(ray, out hitInfo, maxRaycastDepth, potentiallyWalkableLayer);

            if (potentiallyWalkableHit)
            {
                Cursor.SetCursor(walkCursor, cursorHotspot, CursorMode.Auto);

                onMouseOverPotentiallyWalkable(hitInfo.point);
                return true;
            }
            return false;
        }
    }
}