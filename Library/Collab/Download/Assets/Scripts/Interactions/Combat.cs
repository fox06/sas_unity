﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Combat : Interaction
{
    private bool selected = false;
    // private Vector3 target;
    private bool IsInAction;

    private MovementComponent mc;
    private MapObject mo;

    List<GameObject> targets= new List<GameObject>();
    private PlayerSetting player;

    public override void Select()
    {
        selected = true;
     //   mo = GetComponent<MapObject>();

        GetTargets();
    }

    public override void Deselect()
    {
        ClearTargets();

        selected = false;
    }

    private void ClearTargets()
    {
        foreach (var u in targets)
        {
            u.GetComponent<CombatHighlighter>().Deselect();

        }

       targets.Clear();
    }

   

    public void GetTargets()
    {
        ClearTargets();

        foreach (var p in GameEngineManager.Instance.playersSettings)
        {
            if (p == player)
                continue;

            foreach (var unit in p.ActiveUnits)
            {
                var u = unit.GetComponent<CombatHighlighter>();
                targets.Add(unit);
                u.Select();
            }
        }
        
       

       // GameEngineManager.Instance.CalculatePath(mo, out availablePath);
    }



    // Use this for initialization
    void Start()
    {
       player = GetComponent<Player>().playerSetting;

    }

    // Update is called once per frame
    void Update()
    {
        //EventSystem eventSystem = EventSystem.current;

        //if (eventSystem != null && eventSystem.IsPointerOverGameObject())
        //    return;

        //if (selected && Input.GetMouseButtonDown(0) && !IsInAction)
        //{
        //    var sc = GameEngineManager.Instance.ScreenToPointPosition(Input.mousePosition);

        //    var targetNode = MapBase.Instance.GetNodeByWorldPosition(sc);

        //    var target = Vector3.zero;

            //foreach (var node in availablePath)
            //{
            //    if (targetNode == node)
            //    {
            //        target = MapBase.Instance.NodeToWorldCoords(targetNode);
            //        break;
            //    }
            //}

            //if (!availablePath.Contains(targetNode))
            // return;

            //target = MapBase.Instance.NodeToWorldCoords(targetNode);

          //  if (target == Vector3.zero)
          //      return;

            // target = _target.Value;

       //     SendToTarget(target);
        //}

        //if (!IsInAction)
        //    return;

        //if (Vector3.Distance(transform.position, mc.target) < 0.01f)
        //    StopAction();


        //if (mc.target == Vector3.zero)
        //    StopAction();

    }
}
