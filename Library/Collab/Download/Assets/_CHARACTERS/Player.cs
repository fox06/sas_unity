﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public PlayerSetting playerSetting;

    public static PlayerSetting Default;

    void Start()
    {
        playerSetting.ActiveUnits.Add(this.gameObject);
    }

    void OnDestroy()
    {
        playerSetting.ActiveUnits.Remove(this.gameObject);
    }
}
