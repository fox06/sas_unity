﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitStats : Interaction
{
    public string Name;

    public Sprite UnitIcon;

    public string MaxHealth;
    public string CurrentHealth;

    public int MaxShotHoles;
    public int CurrentShotHoles; 

    public string MaxAttackPoints;
    public string CurrentAttackPoints;

    public int MaxMovePoints;
    public int CurrentMovePoints;

    private bool IsShown = false;



    public override void Select()
    {
        IsShown = true;

        InfoManager.Instance.SetTexts(Name, MaxHealth + "/" + CurrentHealth, MaxMovePoints + "/" + CurrentMovePoints);

        InfoManager.Instance.SetUnitIcon(UnitIcon);

    }

    public override void Deselect()
    {
        IsShown = false;

        InfoManager.Instance.ClearTexts();
        InfoManager.Instance.ClearPics();
    }

    public void Reset()
    {
        CurrentAttackPoints = MaxAttackPoints;
        CurrentMovePoints = MaxMovePoints;
    }


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!IsShown)
            return;
        
    }
}
