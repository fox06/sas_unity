﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Combat : Interaction
{
    private bool selected = false;
   // private PlayerSetting player;

    public override void Select()
    {
        selected = true;
        var mo = GetComponent<MapObject>();
        var player = GetComponent<Player>().playerSetting;
        CombatActionManager.Instance.ActivateCombatMode(player, mo);
        
    }

    public override void Deselect()
    {   

        selected = false;

        CombatActionManager.Instance.DeactivateCombatMode();
    }

     // Use this for initialization
    void Start()
    {
         
    }

  
   
}
