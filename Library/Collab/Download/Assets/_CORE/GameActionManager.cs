﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameActionManager : MonoBehaviour {

    public static GameActionManager Instance;

    public Button[] Buttons;

    private List<Action> actionsCalls = new List<Action>();

    void Awake()
    {
        Instance = this;
    }

    public void ClearButtons()
    {
        foreach (var button in Buttons)
        {
            button.gameObject.SetActive(false);
        }

        actionsCalls.Clear();
    }

    public void AddButton(Sprite pic, Action onClick)
    {
        int index = actionsCalls.Count;
        Buttons[index].gameObject.SetActive(true);
        Buttons[index].GetComponent<Image>().sprite = pic;
        actionsCalls.Add(onClick);
    }

    public void OnButtonClick(int index)
    {
        actionsCalls[index]();
    }


    // Use this for initialization
    void Start()
    {

        for (int i = 0; i < Buttons.Length; i++)
        {
            int _ind = i;
            Buttons[_ind].onClick.AddListener(delegate () { OnButtonClick(_ind); });
        }

    //    ClearButtons();
    }
}
