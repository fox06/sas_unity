﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoManager : MonoBehaviour
{
    public static InfoManager Instance;

    public Text TurnPlayerName;

    public Text Txt1;
    public Text Txt2;
    public Text Txt3;

    public Image UnitIconSprite;

    void Awake()
    {
        Instance = this;
    }

    public void SetTexts(string s1, string s2, string s3)
    {
        Txt1.text = s1;
        Txt2.text = s2;
        Txt3.text = s3;
    }

    public void SetUnitIcon(Sprite sprite)
    {
        UnitIconSprite.sprite = sprite;
        UnitIconSprite.color = Color.black;
    }

    public void ClearTexts()
    {
        SetTexts("", "", "");
    }

    public void ClearPics()
    {
        UnitIconSprite.color = Color.clear;
    }

    void Start()
    {
        ClearTexts();
        ClearPics();
    }
}
