﻿using UnityEngine;

public class Highlighter : Interaction
{    
    public GameObject gm;
    public override void Select()
    {
        gm.SetActive(true);
    }

    public override void Deselect()
    {
        gm.SetActive(false);
    }

    void Start()
    {
        gm.SetActive(false);
    }

    void Update()
    {
        
    }

}