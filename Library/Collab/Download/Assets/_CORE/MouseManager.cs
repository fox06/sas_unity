﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseManager : MonoBehaviour
{
    private List<InteractiveObject> SelectedObjects;

    public bool _on = true;

    public static MouseManager Instance = null;

    void Awake()
    {
        Instance = this;
    }

    // Use this for initialization
    void Start () {
		SelectedObjects = new List<InteractiveObject>();
	}
	
	// Update is called once per frame
	void Update () {

        if (!_on)
            return;

	    if (!Input.GetMouseButtonDown(0))
	        return;
	    
        EventSystem eventSystem = EventSystem.current;

        

	    if (eventSystem != null && eventSystem.IsPointerOverGameObject())
	        return;


	    //if (SelectedObjects.Count > 0)
	    //{
	    //    foreach (var interactiveObject in SelectedObjects)
	    //    {
	    //        //interactiveObject.Deselect();
	    //    }
	    //}


	    var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

	    RaycastHit hit;

	    if (!Physics.Raycast(ray,out  hit))
	        return;

	    var inter = hit.transform.GetComponent<InteractiveObject>();



	    if (inter == null || !inter.enabled ) 
	        return;

        var IsUnitOfCurrentUnit = hit.transform.GetComponent<Player>().playerSetting == GameEngineManager.Instance.CurrentPlayerTurn;

        if (!IsUnitOfCurrentUnit)
        {
            return;
        }

        foreach (var interactiveObject in SelectedObjects)
	    {
	        interactiveObject.Deselect();
	    }

        SelectedObjects.Clear();

        SelectedObjects.Add(inter);
	    inter.Select();
	}
}
