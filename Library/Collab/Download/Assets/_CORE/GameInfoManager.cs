﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameInfoManager : MonoBehaviour {

    public static GameInfoManager Instance;

    public Text TurnPlayerName;
    public Text WindType;
    public Text WindDirection;

    void Awake()
    {
        Instance = this;
    }

    public void SetTurnPlayerName(string s1)
    {
        TurnPlayerName.text = s1;
       
    }

    public void SetWindDirection(string s1)
    {
        WindDirection.text = s1;

    }

    public void SetWindType(string s1)
    {
        WindType.text = s1;

    }

    //public void SetUnitIcon(Sprite sprite)
    //{
    //    UnitIconSprite.sprite = sprite;
    //    UnitIconSprite.color = Color.black;
    //}

    //public void ClearTexts()
    //{
    //    SetTexts("", "", "");
    //}

    //public void ClearPics()
    //{
    //    UnitIconSprite.color = Color.clear;
    //}

    void Start()
    {
       // ClearAll();
        //ClearPics();
    }

    private void ClearAll()
    {
       SetTurnPlayerName("");
       SetWindDirection("");
       SetWindType("");
    }
}
