﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackComponent : MonoBehaviour {

    private bool IsInAction = false;

    int attackSpeed = 2;

    public Projectile projectilePrefab;

    public UnitStats targetUnit;

    private float waited;
    private float freq = 2;

    // Use this for initialization
    void Start () {
		
	}

    public void Attack(UnitStats unit, int shootsNumber)
    {

    }
	
	// Update is called once per frame
	void Update () {

        if (targetUnit == null)
            return;

        //if (IsInAction)
        //    return;


        waited += Time.deltaTime;

        if (waited < freq)
            return;

        Vector3 sp = new Vector3(transform.position.x, transform.position.y + .1f, transform.position.z);

        var gm = Instantiate(projectilePrefab, sp, transform.rotation);

        gm.SetTarget(targetUnit);
        //var heading = targetUnit.transform.position - transform.position;

        //Vector3.Normalize(heading);

        waited = 0;

    }
}
