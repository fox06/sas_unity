﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionSelect : Interaction {

    public override void Deselect()
    {
        ActionManager.Instance.ClearButtons();
    }

    public override void Select()
    {
        ActionManager.Instance.ClearButtons();

        foreach (var ab in GetComponents<ActionBehaviour>())    
        {
            ActionManager.Instance.AddButton(ab.pic, ab.GetClickAction());
        }
    }
}
