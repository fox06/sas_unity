﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Movementor : Interaction
{
    private bool selected = false;
   // private Vector3 target;
    private bool IsInAction;

    private MovementComponent mc;
    private MapObject mo;
    UnitStats unitStat;

    List<GameEngineManager.Path> availablePath = new List<GameEngineManager.Path>();

    public override void Select()
    {
        selected = true;
        mo = GetComponent<MapObject>();
        unitStat = GetComponent<UnitStats>();

        CalculatePath();
    }

    public override void Deselect()
    {
        ClearPath();

        selected = false;

        
    }

    private void ClearPath()
    {
        foreach (var node in availablePath)
        {
            node.node.IsMarked = false;
        }

        availablePath.Clear();
    }

    private void SendToTarget(Vector3 target)
    {

        

        IsInAction = true;
        mc.SetDistanation(target);

    }

    public void CalculatePath()
    {
        ClearPath();

        int availableMoves = unitStat.CurrentMovePoints;

        GameEngineManager.Instance.CalculatePath(mo, availableMoves, out availablePath);
    }

    private void StopAction()
    {
        IsInAction = false;
        mc.Stop();

        // CalculatePath();
        GetComponent<InteractiveObject>().Select();
    }

   

// Use this for initialization
	void Start ()
	{
	    mc = GetComponent<MovementComponent>();
	   
	}
	
	// Update is called once per frame
	void Update ()
	{
        if (!MouseManager.Instance._on)
            return;

        EventSystem eventSystem = EventSystem.current;

        if (eventSystem != null && eventSystem.IsPointerOverGameObject())
            return;

        if (selected && Input.GetMouseButtonDown(0) && !IsInAction)
	    {
	        var sc = GameEngineManager.Instance.ScreenToPointPosition(Input.mousePosition);

	        var targetNode = MapBase.Instance.GetNodeByWorldPosition(sc);

            var target = Vector3.zero;

	        foreach (var node in availablePath)
	        {
	            if (targetNode == node.node)
	            {
                    target = MapBase.Instance.NodeToWorldCoords(targetNode);
                    unitStat.CurrentMovePoints -= node.cost;
                    break;
                }
	        }

            //if (!availablePath.Contains(targetNode))
	           // return;

	        //target = MapBase.Instance.NodeToWorldCoords(targetNode);

	        if (target == Vector3.zero)
	            return;

	       // target = _target.Value;

            SendToTarget(target);
        }

	    if (!IsInAction)
	        return;

	    if (Vector3.Distance(transform.position, mc.target) < 0.01f)
	        StopAction();
	    

        if (mc.target == Vector3.zero)
            StopAction();
        
    }
}
