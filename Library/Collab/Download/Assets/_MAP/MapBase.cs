﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapBase : MonoBehaviour
{

    public static MapBase Instance;

    public int xTilesCount;
    public int yTilesCount;
    public int zTilesCount;


    public GameObject debugTile;

    Node[,,] nodeMap;

    bool debugMode = true;
    private int xOffset = 1, yOffset = 1, zOffset = 1;


    public Material unWalkableTile;
    public Material TestTile;

    public Material DefaultMaterial;

    void Awake()
    {
        Instance = this;
    }

    // Use this for initialization
    void Initialization()
    {
        nodeMap = new Node[xTilesCount, yTilesCount, zTilesCount];

        if (debugMode)
        {
            BuildMockMap();
        }
    }

    void BuildMockMap()
    {

        // worldBase.transform.position = Vector3.zero;

        for (int x = 0; x < xTilesCount; x++)
        {
            for (int z = 0; z < zTilesCount; z++)
            {


                for (int y = 0; y < yTilesCount; y++)
                {

                    Node node = new Node(this);
                    node.x = x;
                    node.y = y;
                    node.z = z;
                    node.IsWalkable = true;

                    nodeMap[x, y, z] = node;

                    if (debugMode)
                    {
                        Vector3 position = WorldToNodeCoords(x, y, z);
                        node.gameObject = Instantiate(debugTile, position, Quaternion.identity, transform);
                    }
                }
            }
        }
    }

    public Vector3 WorldToNodeCoords(int x, int y, int z)
    {
        Vector3 result = new Vector3();

        result.x = x * xOffset;
        result.y = y * yOffset;
        result.z = z * zOffset;

        //  Debug.Log(result.ToString());

        return result;
    }

    public Node GetNodeByWorldPosition(Vector3 position)
    {
        int x = Mathf.RoundToInt(position.x);
        int y = Mathf.RoundToInt(position.y);
        int z = Mathf.RoundToInt(position.z);

        Node result = GetNode(x, y, z);

        return result;
    }

    public Node GetNode(int x, int y, int z)
    {
        x = Mathf.Clamp(x, 0, xTilesCount - 1);
        y = Mathf.Clamp(y, 0, yTilesCount - 1);
        z = Mathf.Clamp(z, 0, zTilesCount - 1);

        return nodeMap[x, y, z];
    }

    public Vector3 NodeToWorldCoords(int indexX, int indexY, int indexZ)
    {
        Vector3 result = new Vector3();
        result.x = indexX / xOffset;
        result.y = indexY / yOffset;
        result.z = indexZ / zOffset;

        return result;
    }


    public Vector3 NodeToWorldCoords(Node node)
    {
        Vector3 result = new Vector3();
        result.x = node.x / xOffset;
        result.y = node.y / yOffset;
        result.z = node.z / zOffset;

        return result;
    }


    void Start()
    {
        Initialization();
    }

    
}
