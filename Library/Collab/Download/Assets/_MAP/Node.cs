﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node
{
    public int x, y, z;

    public Node parent;

    public GameObject gameObject;

    MapBase map;

    public Node(MapBase m)
    {
        map = m;
    }


    bool isWalkable;

    bool isMarked;
    // public bool isWalkable;
    public bool IsWalkable
    {
        get { return isWalkable; }

        set
        {

            if (gameObject != null)
            {
                gameObject.GetComponent<Renderer>().material =
                    (value == true) ? map.DefaultMaterial : map.unWalkableTile;
            }

            isWalkable = value;
        }
    }

    public bool IsMarked
    {
        get { return isMarked; }

        set
        {
            if (gameObject != null)
            {
                gameObject.GetComponent<Renderer>().material =
                    (value == true) ? map.TestTile : map.DefaultMaterial ;
            }

            isMarked = value;
        }
    }


    public NodeType nodeType;
    public float hCost;
    public float gCost;
    public float fCost;

    public override string ToString()
    {
        return x + "; " + y + "; " + z;
    }
}

public enum NodeType
{
    Water,
    Ground
}