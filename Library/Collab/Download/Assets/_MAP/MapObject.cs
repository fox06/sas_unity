﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapObject : MonoBehaviour
{
    private Node currentNode;
    private Node prevNode;

    private Vector3 prevPosition;
    public Node CurrentNode { get { return currentNode; }}

    // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (transform.position == prevPosition)
	        return;

        prevPosition = transform.position;

        currentNode = MapBase.Instance.GetNodeByWorldPosition(transform.position);

	    if (currentNode == prevNode)
	        return;

	    currentNode.IsWalkable = false;

        if(prevNode != null)
            prevNode.IsWalkable = true;

	    prevNode = currentNode;

	}
}
