﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SAS.CameraUI;
using System;
using SAS.Map;

public class InteractionSystem : MonoBehaviour {
        
    [SerializeField] GameObject HighlighetTilePrefab;

    [SerializeField] GameObject SelectorPrefab;

    [SerializeField] GameObject EnemySelectorPrefab;

    GameObject Selector;

    GameObject EnemySelector;

    List<GameObject> HighlightedTiles = new List<GameObject>();

    List<GameObject> HighlightedEnemyTiles = new List<GameObject>();

    InteractiveObject selecteUnit = null;

    InteractiveObject selectedEnemy = null;

    List<InteractiveObject> enemiesInRange;

    List<Tile> availablePath = new List<Tile>();

    TileMap map;

    Tile selectedOUnitTile;


    public InteractiveObject SelectedObject
    {
        get
        {
            return selecteUnit;
        }
      
    }

    void Start () {
        CameraRaycaster cameraRaycaster = FindObjectOfType<CameraRaycaster>();

        cameraRaycaster.onMouseOverFriendInteractiveObject += OnMouseOverFriendInteractiveObject;
        cameraRaycaster.onMouseOverEnemyInteractiveObject += OnMouseOverEnemyInteractiveObject;
        cameraRaycaster.onMouseOverPotentiallyWalkable += OnMouseOverPotentiallyWalkable;

        map = FindObjectOfType<TileMap>();

        Selector = Instantiate(SelectorPrefab);
        DeactivateSelector();

        EnemySelector = Instantiate(EnemySelectorPrefab);
        EnemySelector.SetActive(false);
    }

    private void OnMouseOverPotentiallyWalkable(Vector3 destination)
    {
        if (Input.GetMouseButtonDown(0))
        {
            Tile clickedTile = map.GetNodeByWorldPosition(destination);

            if (clickedTile == null)
                return;

            if (selecteUnit != null && availablePath.Contains(clickedTile))
            {
                SelectedObject.transform.position = map.TileCoords(clickedTile._x, clickedTile._y);
            }
            else
            {
                Deselect();
                DeselectEnemy();
            }
        }
    }

    private void OnMouseOverEnemyInteractiveObject(InteractiveObject interactiveObject)
    {
        if (Input.GetMouseButtonDown(0) && SelectedObject != null)
        {
            if(enemiesInRange.Contains(interactiveObject))
                SelectEnemy(interactiveObject);
        }
    }

    private void OnMouseOverFriendInteractiveObject(InteractiveObject interactiveObject)
    {
        if (Input.GetMouseButtonDown(0))
        {
            Select(interactiveObject);
            CalculatePath(5);
            ShowPath(interactiveObject);
            ShowEnemiesInRange();
        }
    }

    void Deselect()
    {
        if (selecteUnit != null)
        {
            foreach (var item in HighlightedTiles)
                Destroy(item);

            HighlightedTiles.Clear();

            foreach (var item in HighlightedEnemyTiles)
                Destroy(item);

            HighlightedEnemyTiles.Clear();

            selecteUnit.Deselect();

            DeactivateSelector();

            selecteUnit = null;

            availablePath.Clear();
        }
    }

    void Select(InteractiveObject interactiveObject) 
    {
        Deselect();
        
        if (interactiveObject != null)
        {
            selecteUnit = interactiveObject;
            selecteUnit.Select();

            selectedOUnitTile = map.GetNodeByWorldPosition(selecteUnit.GetComponent<Transform>().position);

            ActivateSelector();

        }
    }

    void SelectEnemy(InteractiveObject interactiveObject)
    {
        DeselectEnemy();

        if (interactiveObject != null)
        {
            selectedEnemy = interactiveObject;
            EnemySelector.transform.position = interactiveObject.transform.position;
            EnemySelector.SetActive(true);
        }
    }

    void DeselectEnemy()
    {
        if (selectedEnemy != null)
        {
            selectedEnemy.Deselect();
            EnemySelector.SetActive(false);

        }
    }

    void ShowPath(InteractiveObject interactiveObject)
    {
        foreach (var item in HighlightedTiles)
            Destroy(item);
      
        HighlightedTiles.Clear();

        foreach (var tile in availablePath)
        {
            var tileObject = Instantiate(HighlighetTilePrefab, new Vector3(tile._x, 0, tile._y), Quaternion.identity);
            HighlightedTiles.Add(tileObject);
        }        
    }

    void ShowEnemiesInRange()
    {
        foreach (var item in HighlightedEnemyTiles)
            Destroy(item);

        HighlightedEnemyTiles.Clear();

        enemiesInRange = FindEnemies(5);

        foreach (var enemy in enemiesInRange)
        {
            var t = enemy.GetComponent<MapObject>().CurrentNode;

            Vector3 position = map.TileCoords(t._x, t._y);

            var tileObject = Instantiate(EnemySelectorPrefab, position, Quaternion.identity);
            HighlightedEnemyTiles.Add(tileObject);
        }
    }
    
    void CalculatePath(int availableMoves)
    {
        availablePath.Clear();

        if (selectedOUnitTile == null)
            return;
                
        foreach (var wDirection in Wind.GetDirections())
        {
            int maxMoves = Math.Max(0, availableMoves - Wind.GetFactorValue(wDirection));

            for (int i = 1; i <= maxMoves; i++)
            {
                int _x = (int)wDirection.Vector.x * i + selectedOUnitTile._x;
                int _z = (int)wDirection.Vector.z * i + selectedOUnitTile._y;

                if (_x > map.size_x ||
                    _x < 0 ||
                    _z > map.size_z ||
                    _z < 0)
                {
                    break;
                }

                var _currentNode = map.GetTile(_x, _z);

                if (!_currentNode.IsWalkable())
                    break;

                availablePath.Add(_currentNode);
            }
        }
    }

    List<InteractiveObject> FindEnemies(int range)
    {
       var enemiesInRange = new List<InteractiveObject>();

        if (selectedOUnitTile == null)
            return enemiesInRange;
            
        foreach (var playersSetting in GameEngineManager.Instance.playersSettings)
        {
            if (playersSetting == GameEngineManager.Instance.CurrentPlayerTurn)
                continue;

            foreach (var enemyUnit in playersSetting.ActiveUnits)
            {
                MapObject enemyTile = enemyUnit.GetComponent<MapObject>();
                InteractiveObject enemyIO = enemyUnit.GetComponent<InteractiveObject>();


                var disX = selectedOUnitTile._x - enemyTile.CurrentNode._x;
                var disZ = selectedOUnitTile._y - enemyTile.CurrentNode._y;

                if (disX == 0 && Math.Abs(-disZ) <= range)
                    enemiesInRange.Add(enemyIO);

                if (disZ == 0 && Math.Abs(-disX) <= range)
                    enemiesInRange.Add(enemyIO);


                if (-disX <= range)
                    if (disX == disZ)
                        enemiesInRange.Add(enemyIO);

                if (disX >= -range)
                    if (disX == -disZ)
                        enemiesInRange.Add(enemyIO);

            }
            
        }
        
        return enemiesInRange;
    }

    void ActivateSelector()
    {
        if (SelectedObject == null)
            return;

        Selector.SetActive(true);
        Selector.transform.position = SelectedObject.transform.position;
        Selector.transform.parent = SelectedObject.transform;
    }

    void DeactivateSelector()
    { 
        Selector.SetActive(false);
       // SelectedObject.transform.parent = null;
    }
}
